import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-storage',
  templateUrl: './storage.component.html',
  styleUrls: ['./storage.component.css']
})
export class StorageComponent implements OnInit {

  constructor() { }

  nombre!:string;
  nombre2!:string;


  ngOnInit(): void {
    this.nombre = 'Mercedes Sosa';
    this.nombre2 = 'Pedro Meneces';

    //Guardar datos
    // set Item (clave, valor)
    //nombre es la clave
    // sessionStorage.setItem('nombre', this.nombre);

    //recuperara datos , la recuperas en una variable 
    //getItem (clave, valor)
    // let resultado = sessionStorage.getItem('nombre');
    // console.log(resultado);

    //Remover un item del session storage
    //removeItem (clave)
    // sessionStorage.removeItem ('nombre');
    // resultado = sessionStorage.getItem('nombre');
    // console.log(resultado);
    
    // Limpiar todo el storage
    // sessionStorage.clear ();
    // resultado = sessionStorage.getItem('nombre');
    // console.log(resultado);
    

    //Guardar datos
    localStorage.setItem ('nombre2', this.nombre);

    //Recuperar datos 
    // let resultado = localStorage.getItem('nombre2');
    // console.log(resultado);

    // //Remover un item del local storage
    // localStorage.removeItem('nombre2')
    // console.log(resultado);

    //limpiar todo el storage
  localStorage.clear ();
  let resultado = localStorage.getItem('nombre2')
  console.log(resultado);
  

    
    

  
  }

}
