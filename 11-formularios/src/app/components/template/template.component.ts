import { Component, OnInit } from '@angular/core';
import { Ipersona } from 'src/app/interfaces/persona.interfaces';
import { PaisService } from 'src/app/servicios/pais.service';

@Component({
  selector: 'app-template',
  templateUrl: './template.component.html',
  styleUrls: ['./template.component.css']
})
export class TemplateComponent implements OnInit {

  persona: Ipersona = {
    nombre: '',
    apellido: '',
    correo: '',
    pais: '',
    genero: ''
  };
  constructor(private paisService:PaisService) { }

  paises:any[] = [];

  ngOnInit(): void {
    this.paisService.obtenerPaises().subscribe(paises => {
      console.log(paises);
      this.paises = paises;
      //Inserta elemenentos al inicio del array
      this.paises.unshift({
        nombre: 'seleccione...',
        codigo : ' '
      })
      
    });
  }

  guardar():void {
    console.log('envio del submit');
  
    console.log(this.persona.nombre);
    console.log(this.persona.apellido);
    console.log(this.persona.pais);
    
    
    
    
  }

}
