import { Component } from '@angular/core';


//DECORADOR los decoradores son opcionales
@Component({
  selector: 'app-root',//app-root nombre del componente
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
//CLASE sin export es una clase
export class AppComponent {
  title = 'my-app';
  nombre:string = 'Daniela';
  apellido: string = 'Vicente';
}
