import { Component, OnInit } from '@angular/core';
import { Datos } from 'src/app/interfaces/persona.interface';

@Component({
  selector: 'app-formulario',
  templateUrl: './formulario.component.html',
  styleUrls: ['./formulario.component.css']
})
export class FormularioComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }
  dato:Datos ={
    nombre:' ',
    direccion:' '
  };

  ir():void{
    console.log('enviado');
    console.log(this.dato.nombre);
    console.log(this.dato.direccion);
  }
}
