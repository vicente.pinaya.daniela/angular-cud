import { Directive, ElementRef, HostListener, Input} from '@angular/core';

@Directive({
  selector: '[appResaltado]'
})

export class ResaltadoDirective {

  @Input ("appResaltado") nuevoColor!:string;

  constructor( private el :ElementRef) { 
    console.log('Llamado de directiva');
    el.nativeElement.style.backgroundColor= 'yellow'
    
  }

  // console.log(this.nuevoColor);
  



    //Quiero escuchar en este caso el evento mouse enter
  @HostListener('mouseenter') mouseEntro(){
    // this.el.nativeElement.style.backgroundColor = 'yelow'
  }

  



  //Quiero escuchar en este caso el evento mouse leave

  @HostListener ('mouseleave') mouseSalio () {
    this.el.nativeElement.style.backgroundColor = null;
  }

  private resaltar (color:string): void(
    this.el.nativeElement.style.backgroundColor = color)
  )


}
