import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/servicios/data.service';

@Component({
  selector: 'app-box',
  templateUrl: './box.component.html',
  styleUrls: ['./box.component.css']
})
export class BoxComponent implements OnInit {

  nombreSecundario: string = 'Juan Carlos';
  constructor(private dataService: DataService) { }

  ngOnInit(): void {
  }

  cambiarNombre(): void{
    this.dataService.nombre = this.nombreSecundario; //aqui se usa la variable de 'Juan Carlos' y se sobreescribe este nombre sobre 'sin nombre' que es la variable nombre que tiene dataService
    console.log(this.dataService.nombre);
    
  }
}
