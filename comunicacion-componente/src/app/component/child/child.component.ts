import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-child',
  templateUrl: './child.component.html',
  styleUrls: ['./child.component.css']
})
export class ChildComponent implements OnInit {


  mensaje = 'Mensaje del hijo al padre'
  //este input de aqui, es el input de arriba, el input se coloca en el componente hijo.
  @Input() ChildMessage!: string; //como no queremos inicializar la variable, colocamos !
  @Output() EventoMensaje = new EventEmitter<string>() //es el output de arriba_ es una clase u objeto
  //se puso tipo string, porque abajo hace this.mensaje, y la variable mensaje de arriba es string
  constructor() { }

  ngOnInit(): void {
    console.log(this.ChildMessage); //imprime mensaje del padre
    this.EventoMensaje.emit(this.mensaje); //emit, es que vamos a emitir_ El EventEmitter es un objeto por el new, y tiene el metodo emit
    
  }

}
