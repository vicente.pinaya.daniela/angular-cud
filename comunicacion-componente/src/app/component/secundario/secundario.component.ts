import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/servicios/data.service';

@Component({
  selector: 'app-secundario',
  templateUrl: './secundario.component.html',
  styleUrls: ['./secundario.component.css']
})
export class SecundarioComponent implements OnInit {

  direccion:string = "Av. Los Olmos #123"

  constructor(private dataService:DataService) { }

  ngOnInit(): void {
  }

  cambiarDireccion():void {
    this.dataService.ModificarDireccion(this.direccion);
  }

}
