import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  nombre:string = 'sin nombre';
  direccion: string = '';

  private _dataSource = new BehaviorSubject<string>(''); //BehaviorSubject es un objeto de tipo cadena, inicializado vacio

  //asObservable convierte a un observable
  //dataSource$ es una variable para suscribirse a la informacion 
  dataSource$ = this._dataSource.asObservable();
  constructor() { }

  public ModificarDireccion (_direccion: string): void{
    this.direccion = _direccion;
    //con next se coloca un valor al _daraSource (el valor direccion)
    this._dataSource.next(this.direccion);
  }
}

//_dataSource -->dataSource$
