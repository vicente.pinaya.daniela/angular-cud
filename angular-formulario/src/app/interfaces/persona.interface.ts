export interface Ipersona { //se puso persona, porque la informacion sera de una persona
  //aqui va lo que tiene el firmulario
  nombre: string;
  apellido: string;
  correo: string;
  pais: string;
  genero: string
}