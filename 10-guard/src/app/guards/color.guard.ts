import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ColorGuard implements CanActivate {
  constructor (private router:Router){

  }


  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {

      this.router.navigate (['/home'])
    return false; //si es true no bloquea 
    //si retorna false la ruta de color no se cargaria  y obedece la ruta que tu le pones 
    //si es true la ruta normal q es color se cargaria
  }
  
}
