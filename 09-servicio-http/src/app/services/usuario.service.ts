import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import {filter, take, map} from 'rxjs/operators';




@Injectable({
  providedIn: 'root'
})
export class UsuarioService {
  url = 'https://randomuser.me/api/';
 
  

  constructor(public http: HttpClient) { }

  obtenerUsuario():Observable<any>{
    return this.http.get<any>(this.url);
  }

  obtenerUsuarioMasculino():Observable<any>{
    return this.http.get<any> (this.url).
    pipe(
      filter(
        usuario => usuario['results'][0].gender != 'male'
      )
    )
  }

   obtenerUsuarioCanada():Observable<any> {
     return this.http.get<any> (this.url).
     pipe (
       filter(
         usuario =>usuario['results'][0].location['country']!= 'Canada'
          )
  )
   }


   obtenerCantidadElementos():Observable<any>{
     return this.http.get<any>(this.url).
             pipe(
               take(0)// tu le dices cuantos elementos quieres 
             );
  
     
   }


   obtnerfoto(): Observable<any>{
     return this.http.get<any>(this.url).
            pipe(
            map(resp => {
                console.log(resp);
              return resp ['results'].map((usuario:any) => { 
                console.log(usuario);
                return {
                name: usuario.name,
                picture: usuario.picture
                }
              });
            })
          );
        }

  obtenerInformacion(): Observable<any>{
    return this.http.get<any>(this.url).
    pipe (
      map(resp => {
        console.log(resp);
        return resp ['results'].map((usuario4:any) => {
          // console.log(usuario4);
          // return usuario4;

           return {
             name:usuario4.name,
             picture:usuario4.picture,
             email:usuario4.email,
             login:usuario4.login
         


           }
        });
      })
    );

  }



}
