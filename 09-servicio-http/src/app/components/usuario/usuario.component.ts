import { Component, OnInit } from '@angular/core';
import { UsuarioService } from 'src/app/services/usuario.service';

@Component({
  selector: 'app-usuario',
  templateUrl: './usuario.component.html',
  styleUrls: ['./usuario.component.css']
})
export class UsuarioComponent implements OnInit {
  usuario :any;
  usuario2: any;
  femenino:boolean;
  Canada:boolean;
  usuario3:any;
  usuario4:any;

  constructor( private servicioUsuario:UsuarioService) { 
    this.femenino = false;
    this.Canada = false;
  }

  ngOnInit(): void {
    this.servicioUsuario.obtenerUsuario().subscribe({
      next: user => {
        // console.log(user);
        this.usuario = user['results'][0]
      },

      error:error => {
        console.log(error);
      },

      complete:() =>{
        console.log('solicitud completa');
      }

    });
  }


  showFemale(): void{
        this.servicioUsuario.obtenerUsuarioMasculino().subscribe({
          next: user => {
    //         console.log(user); 
            this.usuario2 = user['results'][0];
            this.femenino = true;
  },
    
         
          error: error => {
            console.log(error);
  }

});

}


 showCanada (): void {
   this.servicioUsuario.obtenerUsuarioCanada().subscribe({
     next: user =>{
       this.usuario2 = user['results'][0];
       this.Canada = true;
     },

     error:error => {
       console.log(error);
      
     }
   })
 }

 MostrarElementos():void{
   this.servicioUsuario.obtenerCantidadElementos().
   subscribe({
     next: user =>{
       console.log(user); 
     },

     error:error => {
       console.log(error);
      
     }

   });
 }


 MostrarFoto(): void {
   this.servicioUsuario.obtnerfoto().subscribe({
     next:user =>{
       console.log(user);
       this.usuario3 = user [0];
      
     },

     error:error => {
       console.log(error);
      
     }
   });
 }



Mostrarinformacion ():void {
  this.servicioUsuario.obtenerInformacion().subscribe ({
    next:user => {
      console.log(user);
      this.usuario4 = user [0];
      
    },

    error:error =>{
      console.log(error);
    }
  });
}
}
