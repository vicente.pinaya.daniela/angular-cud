import { Pipe, PipeTransform } from '@angular/core';

@Pipe({  //decorador
  name: 'capitalizado'
})
export class CapitalizadoPipe implements PipeTransform {

  transform(value: string, todas:boolean = true): string {
    console.log(value);
    console.log(todas);


    value = value.toLowerCase();
    let nombres = value.split('');
    console.log(nombres);

    if (todas) {
      nombres = nombres.map(nombre => {
        return nombre [0].toUpperCase()+ nombre.substring(1);
      });
    }else {
      console.log(nombres);
      console.log(nombres[0][0].toUpperCase());
      //primero el elemento , luego el caracter del elemento

      nombres[0] = nombres[0][0].toUpperCase() + nombres [0].substring(1);
      
    }
    // return 'Hola Mundo';
    return nombres.join('');
    
  }

}
