import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-pipes',
  templateUrl: './pipes.component.html',
  styleUrls: ['./pipes.component.css']
})
export class PipesComponent implements OnInit {

  nombre ="ramiro santos";
  nombre2 = " MaxiMiliAno PaReDes "
  array : number [] = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
  //  slice saca los elementos
  PI:number=Math.PI
  porcentaje:number=0.234;
  salario: number= 1234.5;

  fecha:Date = new Date ();

  heroe: any = {
    nombre:'Logan',
    clave :'wolverine',
    edad: 500,
    direccion:{
      calle:'Las vertientes #478',
      zona:'Los Horizontes'
    }
  };




    valorPromesa = new Promise<string>((resolve) => {
      setTimeout(() =>{ //settimeout  PARA PARAR EL TIEMPOhace una espera //pausa de 3500milisegundos y lo que sale es resol
        resolve('llego la data');
      }, 3500); //3500 son los milisegundo que es el tiempo en que sale la cadena
    })



  

  constructor() { }

  ngOnInit(): void {
  }

}

//CAPITALIZAR Colocar en mayuscula
